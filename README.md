# Auto DevOps Image

## Supported deployment strategies
* EC2/PM2 *(deprecated)*
* AWS Elastic Beanstalk
* AWS Elastic Kubernetes Service *(WIP)*

## Quickstart
TODO: add quickstart steps on local environment

## Directories
```
.
├── auto-build-image/               # Build image
│   ├── src/
│   └── Dockerfile
├── auto-deploy-image/              # Deploy image
│   ├── assets/
│   ├── src/
│   └── Dockerfile
├── docs/                           # Document directory
├── DEVELOPMENT.md                  # Development guildlines
└── README.md
```

## Limitations

## [Development guidelines](./DEVELOPMENT.md)

## [Known-issues](./docs/known-issues.md)
