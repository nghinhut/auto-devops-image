#!/bin/bash -e
[[ $TRACE ]] && set -x

export AWS_ACCESS_KEY_ID=${AUTO_DEPLOY_AWS_ACCESS_KEY_ID:-"$AWS_ACCESS_KEY_ID"}
export AWS_SECRET_ACCESS_KEY=${AUTO_DEPLOY_AWS_SECRET_ACCESS_KEY:-"$AWS_SECRET_ACCESS_KEY"}
export AWS_REGION=${AUTO_DEPLOY_AWS_REGION:-"$AWS_REGION"}
export AWS_ACCOUNT_ID=${AUTO_DEPLOY_AWS_ACCOUNT_ID:-"$AWS_ACCOUNT_ID"}

## Internal configs
SHORT_COMMIT_SHA=$(git rev-parse --short HEAD)
LONG_COMMIT_SHA=$(git rev-parse HEAD)
commit_ref_slug=$(git rev-parse --abbrev-ref HEAD)
bin=/build/bin

ensure_awscli() {
  if ! [ -x "$(command -v aws)" ] || ! [ -x "$(command -v eb)" ]; then
    if [ -x "$(command -v apt-get)" ]; then
      apt-get update >/dev/null 2>&1
      apt-get install -y python3-pip >/dev/null 2>&1
    elif [ -x "$(command -v apk)" ]; then
      apk add --no-cache py3-pip >/dev/null 2>&1
      apk add --no-cache python3-dev autoconf automake g++ make openssl-dev libffi-dev
    else
      echo "Unknown package manager" && exit 1
    fi

    # fix awsebcli installation error
    export LANG=C.UTF-8

#    botocore_version=${BOTOCORE_VERSION:-"1.19.63"}
#    awscli_version=${AWS_CLI_VERSION:-"1.18.223"}
#    awsebcli_version=${AWS_EB_CLI_VERSION:-"3.19.3"}
#    pip3 install "botocore==${botocore_version}" "awscli==${awscli_version}" "awsebcli==${awsebcli_version}"
    pip3 install -r /assets/requirements.txt
  fi
  aws configure set default.region "${AWS_REGION}"
  aws configure set region "${AWS_REGION}"
  aws configure set aws_access_key_id "${AWS_ACCESS_KEY_ID}"
  aws configure set aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
}

aws_configure() {
  aws configure set default.region "${AWS_REGION}"
  aws configure set region "${AWS_REGION}"
  aws configure set aws_access_key_id "${AWS_ACCESS_KEY_ID}"
  aws configure set aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
}

docker_login() {
  application_name=${APPLICATION_NAME:-""};
  application_name=${application_name:-"$BITBUCKET_REPO_SLUG"}
  account_id=${AUTO_DEPLOY_AWS_ACCOUNT_ID:-`aws sts get-caller-identity --query Account --output text`}

  docker_registry=${DOCKER_REGISTRY:-"$account_id.dkr.ecr.$AWS_REGION.amazonaws.com/$application_name/$commit_ref_slug"}
#  docker_registry=${DOCKER_REGISTRY:-"$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"}
#  aws ecr --region "$AWS_REGION" get-login-password | docker login --username AWS --password-stdin "$docker_registry"

  # same func as above but run on non interactive mode
  docker login --username AWS -p "$(aws ecr get-login-password --region "$AWS_REGION")" "$docker_registry"
}

_dump_env() {
  COMMON_LINUX_VARS="^HOME=|^HOSTNAME=|^LANG=|^PATH=|^PWD=|^_="
  COMMON_CI_VARS="^CI=|GPG_KEY|DOCKER_HOST|BITBUCKET_|AWS_|KUBERNETES_|PIPELINES_|SSH_|AUTO_DEPLOY_"

  env | grep -vE "$COMMON_LINUX_VARS|$COMMON_CI_VARS" > .env
}

_get_application_name() {
  application_name=${APPLICATION_NAME:-""}

  ## GitLab
  application_name=${application_name:-"$CI_PROJECT_NAME"}

  ## Bitbucket
  application_name=${application_name:-"$BITBUCKET_REPO_SLUG"}

  echo "$application_name"
}

_get_environment() {
  environment=${ENVIRONMENT:-""}

  # GitLab
  environment=${environment:-"$CI_ENVIRONMENT_SLUG"}

  environment=${environment:-"$BITBUCKET_DEPLOYMENT_ENVIRONMENT"}

  echo "$environment"
}

cloudfront() {
  $bin/cloudfront
}

elasticbeanstalk() {
  trap "/build/bin/elasticbeanstalk cleanup" EXIT          # call on exit

  if [[ "$CI" == "true" ]]; then
    _dump_env
  fi

#  if ! [[ -x "$(command -v jq)" ]]; then
#    (apt-get install -y jq >/dev/null 2>&1 || apk add jq >/dev/null 2>&1) || exit 1
#  fi

  export APPLICATION_NAME=$(_get_application_name)
  export ENVIRONMENT=$(_get_environment)

  ## to lowercase and replace / by - character
  EB_ENVIRONMENT=$(echo "$EB_ENVIRONMENT" | tr '[:upper:]' '[:lower:]')
  EB_ENVIRONMENT=${EB_ENVIRONMENT//[\/]/-}

  $bin/elasticbeanstalk check_cname_availability
  $bin/elasticbeanstalk init
  $bin/elasticbeanstalk ensure_environment
  $bin/elasticbeanstalk deploy

  exit 0
}

##
# Remove comments, empty lines, empty values (Copy of same function name in "elasticbeanstalk" script)
# | sed '/^#/ d' | sed '/^$/ d' | sed '/^.*=$/ d'
##
_trim() {
  b=$(mktemp)
  while read d; do
    echo $d >> $b
  done

  cat $b | sed '/^#/ d' | sed '/^$/ d' | sed '/^.*=$/ d'
}
_escape() {
  b=$(mktemp)
  while read d; do
    echo $d >> $b
  done

  file="$b"
  while IFS="=" read -r key value; do
    case "$key" in
      '#'*) ;;
      *)
        # ignore empty value
        if [ -z "$value" ]; then
          continue
        fi

        origin=$value
        format=$value
        format="${format%\"}"
        format="${format#\"}"

        if [ "$format" != "$origin" ]; then
          printf '%s="%s"\n' "$key" "$format"
        else
          printf '%s=%s\n' "$key" "$value"
        fi
    esac
  done < "$file"
}

kubernetes() {
  # TODO: implement additional steps (commented)
#  $bin/kubernetes check_kube_domain
#  $bin/kubernetes download_chart
#  $bin/kubernetes use_kube_context
  $bin/kubernetes ensure_namespace
  $bin/kubernetes create_secret
  $bin/kubernetes deploy
#  $bin/kubernetes delete canary
#  $bin/kubernetes persist_environment_url

  return 0
}

option=${1:-"elasticbeanstalk"}
case $option in
  # helpers
  ensure_awscli) ensure_awscli ;;
  aws_configure) aws_configure ;;
  docker_login) docker_login ;;

  # deploy scripts
  cloudfront) cloudfront ;;
  docker-ssh) docker-ssh ;;
  ecs) ecs ;;
  elasticbeanstalk) elasticbeanstalk ;;
  kubernetes) kubernetes ;;
  pm2) pm2 ;;

  *) exit 1 ;;
esac
