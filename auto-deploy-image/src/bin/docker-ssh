#!/bin/bash -e
[[ $TRACE ]] && set -x

docker-ssh() {
  ssh_user=$SSH_USER
  ssh_host=$SSH_HOST
  ssh_private_key="$SSH_PRIVATE_KEY"
  ssh_known_hosts="$SSH_KNOWN_HOSTS"
  application_name=$(_get_application_name)
  environment=$(_get_environment)
  docker_registry="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"
  docker_repository=${DOCKER_REPOSITORY:-"$application_name/$commit_ref_slug"}
  image_tag="$LONG_COMMIT_SHA"
  docker_image="$docker_registry/$docker_repository:$image_tag"
  remote_workdir="$application_name/$environment"
  assets_dir="/assets/docker-ssh"

  echo "SSH+Docker deployment target"
  precondition_failed=false
  [[ -z "$ssh_user" ]] && echo "SSH_USER is required" && precondition_failed=true
  [[ -z "$ssh_host" ]] && echo "SSH_HOST is required" && precondition_failed=true
  [[ -z "$application_name" ]] && echo "APPLICATION_NAME is required" && precondition_failed=true
  [[ -z "$environment" ]] && echo "ENVIRONMENT is required" && precondition_failed=true
  [[ $precondition_failed == "true" ]] && exit 1

  if ! [[ -x "$(command -v ssh)" ]]; then
    if [[ -x "$(command -v apt-get)" ]]; then
      apt-get update >/dev/null 2>&1
      apt-get install -y openssh-client >/dev/null 2>&1
    elif [[ -x "$(command -v apk)" ]]; then
      apk add --no-cache openssh >/dev/null 2>&1
    else
      echo "Unknown package manager" && exit 1
    fi
  fi

  ## Run ssh-agent (inside the build environment)
  eval "$(ssh-agent -s)"

  if [[ "$ssh_private_key" ]]; then
    echo "$ssh_private_key" | tr -d '\r' | ssh-add -
  fi

  mkdir -p ~/.ssh && chmod 700 ~/.ssh

  if [[ "$ssh_known_hosts" ]]; then
    echo "$ssh_known_hosts" >> ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts
  else
    echo "Warning: set 'StrictHostKeyChecking' to 'no'"
    echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
  fi

  echo -e "Host remote\nHostname $ssh_host\nUser $ssh_user\n\n" >> ~/.ssh/config

  ## Verify SSH login successful
  ssh remote 'exit' || (echo "SSH login failed" && exit 1)

  ## Filter environment variables
  temp=$(mktemp) && env | _trim | grep -E '^DOCKER_SECRET_' | sed -e 's/^DOCKER_SECRET_//' | _escape > "$temp"
  cat "$temp"

  # shellcheck disable=SC2029
  ssh remote "mkdir -p $remote_workdir"
  scp "$temp" "remote:$remote_workdir/.env"
  token=$(aws ecr get-login-password --region "$AWS_REGION")
  ssh remote 'bash -s' <<< "
  docker login --username AWS -p $token $docker_registry
  docker pull $docker_image
  (docker stop $application_name-$environment && docker rm $application_name-$environment) || true
  docker run -itd --name=$application_name-$environment --network=docker-ssh_network -p 5000:5000 --env-file=$remote_workdir/.env --restart=always $docker_image"

  return 0
}
