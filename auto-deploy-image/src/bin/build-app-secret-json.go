//usr/bin/env go run "$0" "$@"; exit

package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strings"
)

const prefixRegex = "^K8S_SECRET_(.*)"

type Secret struct {
	ApiVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name string `json:"name,omitempty" json:"name,omitempty"`
	} `json:"metadata,omitempty"`
	Type string            `json:"type"`
	Data map[string]string `json:"data"`
}

func main() {
	re := regexp.MustCompile(prefixRegex)

	secret := &Secret{
		ApiVersion: "v1",
		Kind:       "Secret",
		Metadata: struct {
			Name string `json:"name,omitempty" json:"name,omitempty"`
		}{Name: os.Getenv("APPLICATION_SECRET_NAME")},
		Type: "Opaque",
		Data: make(map[string]string),
	}

	for _, e := range os.Environ() {
		s := strings.SplitN(e, "=", 2) // split into 2 substrings
		k := s[0]
		v := s[1]

		// Filter by prefix
		match := re.FindStringSubmatch(k)
		if len(match) == 2 {
			k = match[1]
			ev := base64.StdEncoding.EncodeToString([]byte(v)) // base64 encode environment value
			secret.Data[k] = ev
		}
	}

	data, _ := json.Marshal(secret)

	_ = data
	fmt.Println(string(data))

	return
}
