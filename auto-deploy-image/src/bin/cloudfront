#!/bin/bash

set -e

[[ $TRACE ]] && set -x

assets=/assets
build_artifacts=${ARTIFACT_PATH:-build}
application_name=$BITBUCKET_REPO_SLUG
environment_name=$BITBUCKET_DEPLOYMENT_ENVIRONMENT
aws_region=${AUTO_DEPLOY_AWS_REGION:-${AWS_REGION:-${AWS_DEFAUL_REGIONL:-"us-east-1"}}}
terraform_bucket_name="$environment_name-$application_name-terraform"

if [[ ! -d $build_artifacts ]]; then
  echo "Build artifacts directory not found." && exit 1
fi

function ensure_s3_bucket() {
  if [[ $aws_region == "us-east-1" ]]; then
    aws s3api create-bucket --bucket "$terraform_bucket_name" --region "$aws_region" || true
  else
    aws s3api create-bucket --bucket "$terraform_bucket_name" --region "$aws_region" --create-bucket-configuration LocationConstraint="$aws_region" || true
  fi
}

function terraform_apply() {
  workdir=$PWD
  cd $assets/cloudfront || exit 1
  terraform init \
    --backend-config="bucket=$terraform_bucket_name" \
    --backend-config="key=terraform" \
    --backend-config="region=$aws_region"

  # Workaround for "Error: Missing required argument ... The argument "origin.0.domain_name" is required, but no definition was found."
  # see: https://github.com/hashicorp/terraform-provider-aws/issues/13393
  terraform apply --auto-approve \
    -var "domain_name=$CF_DOMAIN" \
    -var "subdomain=$CF_SUBDOMAIN" \
    --target=module.s3_one

  [[ $TRACE ]] && terraform plan \
      -var "domain_name=$CF_DOMAIN" \
      -var "subdomain=$CF_SUBDOMAIN"
  terraform apply --auto-approve \
    -var "domain_name=$CF_DOMAIN" \
    -var "subdomain=$CF_SUBDOMAIN"

  ARTIFACT_S3_BUCKET=$(terraform output -raw s3_bucket)
  DISTRIBUTION_ID=$(terraform output -raw cloudfront_distribution_id)

  cd "$workdir"
  return 0
}

function upload_artifacts() {
  [[ $TRACE ]] && ls -lha "$build_artifacts"
  printf "Uploading artifacts to S3... "
  aws s3 cp "$build_artifacts" "s3://$ARTIFACT_S3_BUCKET/" --recursive --no-progress --quiet
  echo "done."
}

# https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Invalidation.html
function cloudfront_invalidate_files_from_edge_caches() {
  echo "Trigger CloudFront cache invalidation."
  aws cloudfront create-invalidation --distribution-id "$DISTRIBUTION_ID" --paths "/*"
  return 0
}

function success_message() {
  url="$CF_DOMAIN"
  if [[ $CF_SUBDOMAIN != "" ]]; then
    url="$CF_SUBDOMAIN.$CF_DOMAIN"
  fi
  echo "Deploy success!"
  echo "Website available at https://$url"
}

function main() {
  ensure_s3_bucket
  terraform_apply
  upload_artifacts
  cloudfront_invalidate_files_from_edge_caches
  success_message
}
main