provider "aws" {
  region = "us-east-1" # CloudFront expects ACM resources in us-east-1 region only

  # Make it faster by skipping something
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true

  # skip_requesting_account_id should be disabled to generate valid ARN in apigatewayv2_api_execution_arn
  skip_requesting_account_id = false
}

locals {
  url = var.subdomain != "" ? "${var.subdomain}.${var.domain_name}" : var.domain_name
}

module "cloudfront" {
  source = "terraform-aws-modules/cloudfront/aws"

  aliases = [local.url]

  comment             = "${local.url} CloudFront"
  enabled             = true
  is_ipv6_enabled     = true
  price_class         = "PriceClass_200" # "PriceClass_100" | "PriceClass_200" | "PriceClass_All" (see https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PriceClass.html)
  retain_on_delete    = false
  wait_for_deployment = false

  # When you enable additional metrics for a distribution, CloudFront sends up to 8 metrics to CloudWatch in the US East (N. Virginia) Region.
  # This rate is charged only once per month, per metric (up to 8 metrics per distribution).
  create_monitoring_subscription = true

  create_origin_access_identity = true
  origin_access_identities = {
    s3_bucket_one = "${local.url} S3 CloudFront can access"
  }

  logging_config = {
    bucket = module.log_bucket.s3_bucket_bucket_domain_name
    prefix = "cloudfront"
  }

  origin = {
    #    appsync = {
    #      domain_name = "appsync.${var.domain_name}"
    #      custom_origin_config = {
    #        http_port              = 80
    #        https_port             = 443
    #        origin_protocol_policy = "match-viewer"
    #        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    #      }
    #
    #      custom_header = [
    #        {
    #          name  = "X-Forwarded-Scheme"
    #          value = "https"
    #        },
    #        {
    #          name  = "X-Frame-Options"
    #          value = "SAMEORIGIN"
    #        }
    #      ]
    #
    #      origin_shield = {
    #        enabled              = true
    #        origin_shield_region = "us-east-1"
    #      }
    #    }

    s3_one = {
      domain_name = module.s3_one.s3_bucket_website_endpoint
      custom_origin_config = {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "http-only" # S3 only support HTTP
        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }
      # use s3 directly
      #      s3_origin_config = {
      #        origin_access_identity = "s3_bucket_one" # key in `origin_access_identities`
      #        # cloudfront_access_identity_path = "origin-access-identity/cloudfront/E5IGQAA1QO48Z" # external OAI resource
      #      }
    }
  }

  #  origin_group = {
  #    group_one = {
  #      failover_status_codes      = [403, 404, 500, 502]
  #      primary_member_origin_id   = "appsync"
  #      secondary_member_origin_id = "s3_one"
  #    }
  #  }

  ## Deprecated: Use S3 Static Web Hosting to support redirect (/) to /index.html in sub-directory
  #  custom_error_response = [
  #    {
  #      error_caching_min_ttl: "0"
  #      error_code: "400"
  #      response_page_path: "/index.html"
  #      response_code: "200"
  #    },
  #    {
  #      error_caching_min_ttl: "0"
  #      error_code: "403"
  #      response_page_path: "/index.html"
  #      response_code: "200"
  #    },
  #    {
  #      error_caching_min_ttl: "0"
  #      error_code: "404"
  #      response_page_path: "/index.html"
  #      response_code: "200"
  #    }
  #  ]

  default_cache_behavior = {
    target_origin_id       = "s3_one" # key in `origin` above
    viewer_protocol_policy = "redirect-to-https"

    ## https://us-east-1.console.aws.amazon.com/cloudfront/v3/home?region=us-east-1#/policies/cache/658327ea-f89d-4fab-a63d-7e88639e58f6
    default_ttl = 86400
    min_ttl     = 1
    max_ttl     = 31536000

    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
    query_string    = true

    #    cache_policy_id          = data.aws_cloudfront_cache_policy.this.id
    #    function_association = {
    #      viewer-request = {
    #        function_arn = aws_cloudfront_function.viewer_request.arn
    #      }
    #    }
  }

  #  default_root_object = "index.html"

  #  default_cache_behavior {
  #    allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
  #    cached_methods = ["GET", "HEAD"]
  #    target_origin_id = "S3-${aws_s3_bucket.prod.bucket}"
  #    # Forward all query strings, cookies and headers
  #    forwarded_values {
  #      query_string = true
  #    }
  #    viewer_protocol_policy = "allow-all"
  #    min_ttl = 0
  #    default_ttl = 3600
  #    max_ttl = 86400
  #  }

  #  default_cache_behavior = {
  #    target_origin_id       = "appsync"
  #    viewer_protocol_policy = "allow-all"
  #    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
  #    cached_methods         = ["GET", "HEAD"]
  #    compress               = true
  #    query_string           = true
  #
  #    # This is id for SecurityHeadersPolicy copied from https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-response-headers-policies.html
  #    response_headers_policy_id = "67f7725c-6f97-4210-82d7-5512b31e9d03"
  #
  #    lambda_function_association = {
  #
  #      # Valid keys: viewer-request, origin-request, viewer-response, origin-response
  #      viewer-request = {
  #        lambda_arn   = module.lambda_function.lambda_function_qualified_arn
  #        include_body = true
  #      }
  #
  #      origin-request = {
  #        lambda_arn = module.lambda_function.lambda_function_qualified_arn
  #      }
  #    }
  #  }

  #  ordered_cache_behavior = [
  #    {
  #      path_pattern           = "/*"
  #      target_origin_id       = "s3_one"
  #      viewer_protocol_policy = "redirect-to-https"
  #
  #      allowed_methods = ["GET", "HEAD", "OPTIONS"]
  #      cached_methods  = ["GET", "HEAD"]
  #      compress        = true
  #      query_string    = true
  #
  #      function_association = {
  #        # Valid keys: viewer-request, viewer-response
  #        viewer-request = {
  #          function_arn = aws_cloudfront_function.example.arn
  #        }
  #
  #        viewer-response = {
  #          function_arn = aws_cloudfront_function.example.arn
  #        }
  #      }
  #    }
  #  ]

  viewer_certificate = {
    acm_certificate_arn = module.acm.acm_certificate_arn
    ssl_support_method  = "sni-only"
  }

  geo_restriction = {
    #    restriction_type = "whitelist"
    #    locations        = ["NO", "UA", "US", "GB"]
    restriction_type = "none"
  }

  depends_on = [module.s3_one]
}

######
# ACM
######

data "aws_route53_zone" "this" {
  name = var.domain_name
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  domain_name               = var.domain_name
  zone_id                   = data.aws_route53_zone.this.id
  subject_alternative_names = [local.url]
}

#############
# S3 buckets
#############

data "aws_canonical_user_id" "current" {}

## Static Website Hosting
module "s3_one" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 2.0"

  bucket = local.url
  acl    = "public-read"

  website = {
    index_document = "index.html"
    error_document = "index.html" # or use a dedicated page ex: error.html
  }

#  force_destroy = true
}

module "log_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 2.0"

  bucket = "${local.url}-logs"
  acl    = null
  grant = [{
    type        = "CanonicalUser"
    permissions = ["FULL_CONTROL"]
    id          = data.aws_canonical_user_id.current.id
    }, {
    type        = "CanonicalUser"
    permissions = ["FULL_CONTROL"]
    id          = "c4c1ede66af53448b93c283ce9448c4ba468c9432aa01d700d3878632f77d2d0"
    # Ref. https://github.com/terraform-providers/terraform-provider-aws/issues/12512
    # Ref. https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/AccessLogs.html
  }]
  force_destroy = true
}

#############################################
# Using packaged function from Lambda module
#############################################

#locals {
#  package_url = "https://raw.githubusercontent.com/terraform-aws-modules/terraform-aws-lambda/master/examples/fixtures/python3.8-zip/existing_package.zip"
#  downloaded  = "downloaded_package_${md5(local.package_url)}.zip"
#}
#
#resource "null_resource" "download_package" {
#  triggers = {
#    downloaded = local.downloaded
#  }
#
#  provisioner "local-exec" {
#    command = "curl -L -o ${local.downloaded} ${local.package_url}"
#  }
#}

#module "lambda_function" {
#  source  = "terraform-aws-modules/lambda/aws"
#  version = "~> 2.0"
#
#  function_name = "${random_pet.this.id}-lambda"
#  description   = "My awesome lambda function"
#  handler       = "index.lambda_handler"
#  runtime       = "python3.8"
#
#  publish        = true
#  lambda_at_edge = true
#
#  create_package         = false
#  local_existing_package = local.downloaded
#
#  # @todo: Missing CloudFront as allowed_triggers?
#
#  #    allowed_triggers = {
#  #      AllowExecutionFromAPIGateway = {
#  #        service = "apigateway"
#  #        arn     = module.api_gateway.apigatewayv2_api_execution_arn
#  #      }
#  #    }
#}

##########
# Route53
##########

module "records" {
  source  = "terraform-aws-modules/route53/aws//modules/records"
  version = "2.0.0" # @todo: revert to "~> 2.0" once 2.1.0 is fixed properly

  zone_id = data.aws_route53_zone.this.zone_id

  records = [
    {
      name = var.subdomain
      type = "A"
      alias = {
        name    = module.cloudfront.cloudfront_distribution_domain_name
        zone_id = module.cloudfront.cloudfront_distribution_hosted_zone_id
      }
    },
  ]
}

###########################
# Origin Access Identities
###########################
data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${module.s3_one.s3_bucket_arn}/*"]

    ## https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteAccessPermissionsReqd.html
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    # Only allow CloudFront to access
    #    principals {
    #      type        = "AWS"
    #      identifiers = module.cloudfront.cloudfront_origin_access_identity_iam_arns
    #    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = module.s3_one.s3_bucket_id
  policy = data.aws_iam_policy_document.s3_policy.json
}

########
# Extra
########

resource "random_pet" "this" {
  length = 2
}

#resource "aws_cloudfront_function" "example" {
#  name    = "${local.url}-example"
#  runtime = "cloudfront-js-1.0"
#  code    = file("${path.module}/function.js")
#}
#
#resource "aws_cloudfront_function" "viewer_request" {
#  name = "${local.url}-viewer_request"
#  runtime = "cloudfront-js-1.0"
#  publish = true
#  code = file("${path.module}/viewer-request.js")
#}

#module "cdn" {
#  source = "terraform-aws-modules/cloudfront/aws"
#
#  aliases = ["cdn.example.com"]
#
#  comment             = "My awesome CloudFront"
#  enabled             = true
#  is_ipv6_enabled     = true
#  price_class         = "PriceClass_All"
#  retain_on_delete    = false
#  wait_for_deployment = false
#
#  create_origin_access_identity = true
#  origin_access_identities = {
#    s3_bucket_one = "My awesome CloudFront can access"
#  }
#
#  logging_config = {
#    bucket = "logs-my-cdn.s3.amazonaws.com"
#  }
#
#  origin = {
#    something = {
#      domain_name = "something.example.com"
#      custom_origin_config = {
#        http_port              = 80
#        https_port             = 443
#        origin_protocol_policy = "match-viewer"
#        origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
#      }
#    }
#
#    s3_one = {
#      domain_name = "my-s3-bycket.s3.amazonaws.com"
#      s3_origin_config = {
#        origin_access_identity = "s3_bucket_one"
#      }
#    }
#  }
#
#  default_cache_behavior = {
#    target_origin_id           = "something"
#    viewer_protocol_policy     = "allow-all"
#
#    allowed_methods = ["GET", "HEAD", "OPTIONS"]
#    cached_methods  = ["GET", "HEAD"]
#    compress        = true
#    query_string    = true
#  }
#
#  ordered_cache_behavior = [
#    {
#      path_pattern           = "/static/*"
#      target_origin_id       = "s3_one"
#      viewer_protocol_policy = "redirect-to-https"
#
#      allowed_methods = ["GET", "HEAD", "OPTIONS"]
#      cached_methods  = ["GET", "HEAD"]
#      compress        = true
#      query_string    = true
#    }
#  ]
#
#  viewer_certificate = {
#    acm_certificate_arn = "arn:aws:acm:us-east-1:135367859851:certificate/1032b155-22da-4ae0-9f69-e206f825458b"
#    ssl_support_method  = "sni-only"
#  }
#}