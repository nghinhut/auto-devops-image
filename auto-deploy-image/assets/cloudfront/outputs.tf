output "cloudfront_distribution_id" {
  value = module.cloudfront.cloudfront_distribution_id
}

output "s3_bucket" {
  value = module.s3_one.s3_bucket_id
}