container_commands:
  01_mark_leader:
    command: |
      mkdir -p /opt/elasticbeanstalk/etc/
      touch /opt/elasticbeanstalk/etc/leader_only
    leader_only: true

##
# full option list: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
##
option_settings:
  # VPC
  - namespace: aws:ec2:vpc
    option_name: VPCId
    value: ${AUTO_DEPLOY_VPC_ID}

  #  Assign public IP for EC2 instance (default to false)
  - namespace: aws:ec2:vpc
    option_name: AssociatePublicIpAddress
    value: ${AUTO_DEPLOY_ASSOCIATE_PUBLIC_IP_ADDRESS}

  - namespace: aws:ec2:vpc
    option_name: ELBScheme
    value: ${AUTO_DEPLOY_VPC_ELB_SCHEME}

  - namespace: aws:ec2:vpc
    option_name: Subnets
    value: ${AUTO_DEPLOY_VPC_SUBNETS}

  - namespace: aws:ec2:vpc
    option_name: ELBSubnets
    value: ${AUTO_DEPLOY_VPC_ELB_SUBNETS}

  # see https://aws.amazon.com/ec2/instance-types/
  - namespace: aws:ec2:instances
    option_name: InstanceTypes
    value: ${AUTO_DEPLOY_EC2_INSTANCE_TYPES}

  - namespace: aws:elasticbeanstalk:environment
    option_name: LoadBalancerType
    value: ${AUTO_DEPLOY_LOAD_BALANCER_TYPE}

    ## Default process
  - namespace: aws:elasticbeanstalk:environment:process:default
    option_name: HealthCheckPath
    value: ${AUTO_DEPLOY_HEALTH_CHECK_PATH}
  - namespace: aws:elasticbeanstalk:environment:process:default
    option_name: MatcherHTTPCode
    value: 200
  - namespace: aws:elasticbeanstalk:environment:process:default
    option_name: Port
    value: 80
  - namespace: aws:elasticbeanstalk:environment:process:default
    option_name: Protocol
    value: HTTP
  - namespace: aws:elasticbeanstalk:environment:process:default
    option_name: HealthyThresholdCount
    value: 3
  - namespace: aws:elasticbeanstalk:environment:process:default
    option_name: UnhealthyThresholdCount
    value: 2

#  - namespace: aws:elasticbeanstalk:environment:process:metrics
#    option_name: HealthCheckPath
#    value: /metrics
#  - namespace: aws:elasticbeanstalk:environment:process:metrics
#    option_name: MatcherHTTPCode
#    value: 200
#  - namespace: aws:elasticbeanstalk:environment:process:metrics
#    option_name: Port
#    value: 9464
#  - namespace: aws:elasticbeanstalk:environment:process:metrics
#    option_name: Protocol
#    value: HTTP

  ## Listener
  - namespace: aws:elbv2:listener:80
    option_name: DefaultProcess
    value: "default"
  - namespace: aws:elbv2:listener:80
    option_name: ListenerEnabled
    value: true
  - namespace: aws:elbv2:listener:80
    option_name: Protocol
    value: HTTP

  - namespace: aws:elbv2:listener:443
    option_name: DefaultProcess
    value: "default"
  - namespace: aws:elbv2:listener:443
    option_name: ListenerEnabled
    value: true
  - namespace: aws:elbv2:listener:443
    option_name: Protocol
    value: HTTPS
  - namespace: aws:elbv2:listener:443
    option_name: SSLCertificateArns
    value: ${AUTO_DEPLOY_SSL_CERTIFICATE_ARNS}
  - namespace: aws:elbv2:listener:443
    option_name: SSLPolicy
    value: ${AUTO_DEPLOY_SSL_POLICY}

  - namespace: aws:autoscaling:launchconfiguration
    option_name: SecurityGroups
    value: ${AUTO_DEPLOY_AWS_SECURITY_GROUPS}

  ## Load balancer (application)
  - namespace: aws:elbv2:loadbalancer
    option_name: IdleTimeout
    value: 60

  # health check path
#  - namespace: aws:elasticbeanstalk:application
#    option_name: Application Healthcheck URL
#    value: ${AUTO_DEPLOY_HEALTH_CHECK_PATH}

  # Auto Scaling configs (https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-autoscalingasg)
  - namespace: aws:autoscaling:asg
    option_name: MinSize
    value: ${AUTO_DEPLOY_MIN_REPLICAS}
  - namespace: aws:autoscaling:asg
    option_name: MaxSize
    value: ${AUTO_DEPLOY_MAX_REPLICAS}

  # Rolling update configs (https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-autoscalingupdatepolicyrollingupdate)
  - namespace: aws:autoscaling:updatepolicy:rollingupdate
    option_name: RollingUpdateEnabled
    value: true
  - namespace: aws:autoscaling:updatepolicy:rollingupdate
    option_name: RollingUpdateType
    value: Health
  - namespace: aws:autoscaling:updatepolicy:rollingupdate
    option_name: MinInstancesInService
    value: 1

  # https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-elbhealthcheck
  # deprecated: since we using application load balancer
#  - namespace: aws:elb:healthcheck
#    option_name: HealthyThreshold
#    value: 3
#  - namespace: aws:elb:healthcheck
#    option_name: Interval
#    value: 10
#  - namespace: aws:elb:healthcheck
#    option_name: Timeout
#    value: 3
#  - namespace: aws:elb:healthcheck
#    option_name: UnhealthyThreshold
#    value: 2

  # https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/AWSHowTo.cloudwatchlogs.html#AWSHowTo.cloudwatchlogs.streaming
  - namespace: aws:elasticbeanstalk:cloudwatch:logs
    option_name: StreamLogs
    value: true

  # Container's environment variables will be injected bellow
  ## for example:
  #- option_name: ENV_VAR_KEY
  #  value: my-value
