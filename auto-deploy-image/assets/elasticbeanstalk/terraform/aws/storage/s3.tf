variable "name" {
  type = string
}

resource "aws_s3_bucket" "public_bucket" {
  bucket = "${var.name}-public-bucket"
  acl    = "public-read"

  tags = {
    Name = "${var.name}-public-bucket"
  }
}

resource "aws_s3_bucket" "private_bucket" {
  bucket = "${var.name}-private-bucket"
  acl    = "private"

  tags = {
    Name = "${var.name}-private-bucket"
  }
}