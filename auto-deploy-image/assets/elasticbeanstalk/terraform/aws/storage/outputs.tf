output "public_bucket" {
  value = aws_s3_bucket.public_bucket.bucket
}

output "private_bucket" {
  value = aws_s3_bucket.private_bucket.bucket
}