# --- networking/variable.tf ---
variable "name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "public_subnet_count" {
  type = number
}

variable "public_cidrs" {
  type = list(string)
}

variable "private_subnet_count" {
  type = number
}

variable "private_cidrs" {
  type = list(string)
}

variable "max_subnets" {
  type = number
}