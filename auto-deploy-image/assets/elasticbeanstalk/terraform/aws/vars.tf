variable "aws_region" {
  default = ""
}

variable "instance_count" {
  default = 2
}

variable "application_name" {
  type    = string
  default = ""
}

variable "track" {
  type    = string
  default = "stable"
}