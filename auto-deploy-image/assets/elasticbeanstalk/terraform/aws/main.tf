# --- main.tf ---


module "networking" {
  name                 = local.trackable_app_name
  source               = "networking"
  vpc_cidr             = "10.123.0.0/16"
  public_subnet_count  = 2
  private_subnet_count = 3
  max_subnets          = 20
  public_cidrs         = [for i in range(2, 255, 2) : cidrsubnet("10.123.0.0/16", 8, i)]
  private_cidrs        = [for i in range(1, 255, 2) : cidrsubnet("10.123.0.0/16", 8, i)]
}

module "storage" {
  source = "storage"
  name   = local.trackable_app_name
}
//
//module "compute" {
//  source   = "./compute"
//  instance_count = var.instance_count
//}

//resource "aws_instance" "" {
//  ami = ""
//  instance_type = ""
//}