# --- outputs.tf ---
output "vpc_id" {
  value = module.networking.vpc_id
}

output "public_subnet_ids" {
  value = join(",", module.networking.public_subnets[*].id)
}

output "private_subnet_ids" {
  value = join(",", module.networking.private_subnets[*].id)
}

output "s3_public_bucket" {
  value = module.storage.public_bucket
}

output "s3_private_bucket" {
  value = module.storage.private_bucket
}