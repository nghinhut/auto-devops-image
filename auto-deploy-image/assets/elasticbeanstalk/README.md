
## Nginx
```
ebextensions/        contains nginx config for legacy stubs
nginx/               contains latest config for Multi-conatiners platform
```

## Required steps
1. Create VPC
1. Create Subnets -> Update Route table
1. Peering
1. Config Load balancer
1. Register Route 53 
    1. Register subdomain


**Notes:
1. Deploy templates & `.ebignore` file is needed in order to deploy success.
