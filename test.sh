#!/bin/bash -e


old_test() {
  export PATH="$PATH:$PWD/src/bin/"
  export DOCKER_REPOSITORY_NAME=test

  ## export .env
  #source .env
  set -o allexport
  source .env
  set +o allexport

  ./src/bin/auto_build aws_configure
  DOCKER_REGISTRY=public.ecr.aws ./src/bin/auto_build docker_login
  ./src/bin/auto_build
}

test_validation() {
  export A=1
  export B=2
  list="A B C D E F"

  for value in $list
  do
    if [[ -z "$(eval echo "\$$value")" ]]; then
      echo "$value not defined"
    fi
  done
}
test_validation
