

## EKS

### 1. Create node role
see https://docs.aws.amazon.com/eks/latest/userguide/create-node-role.html


## Docker via SSH (docker-ssh)
### Public docker container with Nginx
> Write /srv/docker-ssh_nginx/default.conf
```
# /etc/nginx/conf.d/default.conf

server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
```



```
docker network create -d bridge docker-ssh_network
docker run -itd --network=docker-ssh_network \
    -p 80:80 -p 443:443 \
    -v /srv/docker-ssh_nginx/default.conf:/etc/nginx/conf.d/default.conf \
    -v /srv/docker-ssh_nginx/ssl:/etc/ssl/docker-ssh_nginx \
    --name docker-ssh_nginx nginx:1.18-alpine
```

### Install Docker Compose 
see https://docs.docker.com/compose/install/#install-compose-on-linux-systems
