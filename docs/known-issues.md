## Known-issues
### AWS Elastic Beanstalk environment value cannot greater than 4096
```
ERROR   Service:AmazonCloudFormation, Message:Template format error: Parameter 'EnvironmentVariables' default value '[****]' length is greater than 4096.
```

https://stackoverflow.com/questions/54344236/environmentvariables-default-value-length-is-greater-than-4096
