#!/bin/bash -e
export $(grep -v '^#' .env.staging | xargs)
source <(kubectl completion bash)
source <(helm completion bash)

#echo "Cleanup old state"
#rm -f ./.terraform/terraform.tfstate

#terraform init --upgrade --backend-config="bucket=$TERRAFORM_S3_BUCKET" --backend-config="key=$TERRAFORM_S3_KEY" --backend-config="region=ap-northeast-1"
#terraform refresh \
#  -var "aws_region=$AWS_REGION" \
#  -var "name=$NAME" \
#  -var "track=$ENVIRONMENT" \
#  -var "vpc_cidr=$VPC_CIDR" \
#  -var "vpc_private_subnets=$VPC_PRIVATE_SUBNETS" \
#  -var "vpc_public_subnets=$VPC_PUBLIC_SUBNETS"
#terraform output --raw kubeconfig > .kubeconfig
export KUBECONFIG=.kubeconfig
