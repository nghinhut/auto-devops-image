# Kubernetes

## Set up environment
Your `.env` file
```shell
KUBECONFIG=<.kubeconfig file>
AWS_REGION=ap-northeast-1
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
```

then

```shell
export $(grep -v '^#' .env | xargs)
source <(kubectl completion bash)
source <(helm completion bash)
```

## auto-deploy-chart
```shell
export chart=./auto-deploy-image/assets/auto-deploy-chart/
```


Dry run
```shell
helm upgrade --install hello $chart -f values.yaml --dry-run
```

# Testing
```shell
docker run --rm -it \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v `pwd`:/src \
  --workdir=/src \
  docker:20.10.3 sh -c "apk add bash git && ./test.sh"
```

# Releasing

### 1. Tagging
```shell
git tag -a v0.1.0 -m "First release"
git push origin v0.1.0
```
